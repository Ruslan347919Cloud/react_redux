import React from 'react';
import {Route, Switch} from 'react-router-dom';
import Header from './components/Header/';
import HeaderCms from './components/Header/HeaderCms';
import Footer from './components/Footer/';
import Articles from './components/Articles/';
import Sidebar from './components/Sidebar/';
import Single from './components/Single/';
import Sort from './components/Sort/';
import SearchResult from './components/Header/SearchResult/';
import About from './components/About/';
import UserForm from './components/Admin/User/UserForm/';
import CategoryForm from './components/Admin/Category/CategoryForm/';
import ArticleForm from './components/Admin/Article/ArticleForm/';
import TagForm from './components/Admin/Tag/TagForm/';
import CommentForm from './components/Admin/Comment/CommentForm/';
import List from './components/Admin/List';
import UserView from './components/Admin/User/UserView/';
import CategoryView from './components/Admin/Category/CategoryView/';
import ArticleView from './components/Admin/Article/ArticleView/';
import SetAttributes from './components/Admin/Article/ArticleView/SetAttributes';
import TagView from './components/Admin/Tag/TagView/';
import CommentView from './components/Admin/Comment/CommentView/';
import AdminDefault from './components/Admin/AdminDefault';
import Login from './components/Auth/Login/';
import Signup from './components/Auth/Signup/';
import 'bootstrap/dist/css/bootstrap.css';
import './styles/App.css';
import 'bootstrap/dist/js/bootstrap.min.js';

const App = props => {
  return (
    <React.Fragment>
      <Switch>
        <Route path="/admin" component={HeaderCms} />
        <Route path="/" component={Header} />
      </Switch>
      <div className="wrapper">
        <div className="container-fluid content">
          <div className="row">
            <Switch>
              <Route exact path="/" render={() => <Articles/>} />
              <Route path="/articles/:page" component={Articles} />
              <Route path="/single/:id/:page?" component={Single} />
              <Route path="/sort/:model/:id/:page?" component={Sort} />
              <Route path="/search/:keyword" component={SearchResult} />
              <Route path="/admin/user/form/:id?" component={UserForm} />
              <Route path="/admin/:itemName/index/:page?" component={List} />
              {/*itemName: article, category, comment, tag, user*/}
              <Route path="/admin/user/view/:id/:page?" component={UserView} />
              <Route path="/admin/category/view/:id/:page?" component={CategoryView} />
              <Route path="/admin/category/form/:id?" component={CategoryForm} />
              <Route path="/admin/article/view/:id/:page?" component={ArticleView} />
              <Route path="/admin/article/form/:id?" component={ArticleForm} />
              {/*<Route path="/admin/article/set/:attribute/:id" render={
                props => (<SetAttributes location={props.location} {...props}/>)
              }/>*/}
              <Route path="/admin/article/set/:attribute/:id" component={SetAttributes}/>
              <Route path="/admin/tag/view/:id/:page?" component={TagView} />
              <Route path="/admin/tag/form/:id?" component={TagForm} />
              <Route path="/admin/comment/view/:id/:page?" component={CommentView} />
              <Route path="/admin/comment/form/:id?" component={CommentForm} />
              <Route path="/admin" component={AdminDefault} />
              <Route path="/auth/login" component={Login} />
              <Route path="/auth/signup" component={Signup} />
            </Switch>
            <Sidebar/>
          </div>
        </div>
        <Footer />
      </div>
    </React.Fragment>
  );
}

export default App;