import React from 'react';
import PropTypes from 'prop-types';
import {Redirect} from 'react-router-dom';

export const withForm = Component => {

	class FormComponent extends React.Component {

		componentDidMount() {
			const {id} = this.props.match.params;
			const {findItem, itemName} = this.props;

			if (typeof id !== 'undefined')
				findItem(id, itemName);
		}

		componentWillUnmount() {
			this.props.clearForm();
		}

		submitHandler = formData => {
			const {
				itemName,
				updatingItemId,
				createItem,
				updateItem
			} = this.props;
			
			if (!updatingItemId)
				createItem(formData, itemName);
			else {
				formData.id = updatingItemId;
				updateItem(formData, itemName);
			}
		}

		formIsSubmitted() {
			const {newItemData} = this.props;

			return ((Object.keys(newItemData).length !== 0)
				&& (typeof newItemData.id !== 'undefined'));
		}

		render() {
			const {itemName, newItemData, initialValues} = this.props;
			
			if (this.formIsSubmitted())
				return <Redirect to={`/admin/${itemName}/view/${newItemData.id}`}/>;
			else return (
				<div className="col-md-7 col-lg-8 articles">
					<Component
						onSubmit={this.submitHandler}
						initialValues={initialValues}
					/>
				</div>
			);
		}
	}

	FormComponent.propTypes = {
		itemName: PropTypes.string.isRequired,
		match: PropTypes.object,
		updatingItemId: PropTypes.string.isRequired,
		newItemData: PropTypes.object.isRequired,
		createItem: PropTypes.func.isRequired,
		updateItem: PropTypes.func.isRequired,
		findItem: PropTypes.func.isRequired,
		clearForm: PropTypes.func.isRequired
	};

	return FormComponent;
}