import React from 'react';
import PropTypes from 'prop-types';
import {Link, Redirect} from 'react-router-dom';
import Pagination from '../Pagination/';

export const withList = Component => {

	class ListComponent extends React.Component {

		constructor(props) {
			super(props);
			this.pageSize = 3;
		}

		componentDidMount() {
			const {itemName} = this.props.match.params;
			const {fetchItems} = this.props;
			const page = this.getCurrentPage();
			fetchItems(page, this.pageSize, itemName);
		}

		componentDidUpdate(prevProps) {
			const {itemName} = this.props.match.params;
			const {
				itemName: itemNamePrev
			} = prevProps.match.params;
			const {
				deletedItemId,
				changedItem,
				fetchItems
			} = this.props;
			const {
				deletedItemId: deletedItemIdPrev,
				changedItem: changedItemPrev
			} = prevProps;
			const page = this.getCurrentPage();
			const pagePrev = (typeof prevProps.match.params.page !== 'undefined')
				? (Number(prevProps.match.params.page)) : 1;
			if (page !== pagePrev ||
				deletedItemId !== deletedItemIdPrev ||
					changedItem !== changedItemPrev ||
						itemName !== itemNamePrev)
						fetchItems(page, this.pageSize, itemName);
		}

		getComponentsList() {
			const {itemName} = this.props.match.params;
			const {
				items,
				deleteItem,
				changeItemStatus
			} = this.props;
			return items.map(item => (
				<Component
					itemName={itemName}
					item={item}
					key={item.id}
					deleteItem={deleteItem}
					changeItemStatus={changeItemStatus}
				/>
			));
		}

		getHeader() {
			const {items} = this.props;
			const itemKeys = (items.length > 0)
				? Object.keys(items[0]) : null;
			const headerFields = (itemKeys !== null)
				? (itemKeys.map(item => (
					<th scope="col" key={itemKeys.indexOf(item)}>
						{item}
					</th>
				))) : null ;
			return (
				<thead className="thead-dark">
					<tr>
						{headerFields}
						<th scope="col">actions</th>
					</tr>
				</thead>
			);
		}

		getCurrentPage() {
			return (typeof this.props.match.params.page !== 'undefined')
				? Number(this.props.match.params.page) : 1;
		}

		firstLetterToUppercase(word) {
			return word.charAt(0).toUpperCase() + word.slice(1);
		}

		render() {
			const {itemName, page} = this.props.match.params;
			const {pages} = this.props;
			if (pages.length > 0 && page > pages.length)
				return <Redirect to={`/admin/${itemName}/index/${pages.length}`}/>;
			else return (
				<div className="col-md-12 articles">
					<h2>{this.firstLetterToUppercase(itemName)}</h2>
					<Link className="btn btn-success btn-lg mb-2" to={`/admin/${itemName}/form`}>
						Create {itemName}
					</Link>
					<table className="table table-bordered mb-4 shadow rounded">
						{this.getHeader()}
					  {this.getComponentsList()}
					</table>
					{(pages.length > 1) && (<Pagination
				  	pages={pages}
						currentPage={this.getCurrentPage()}
						route={`/admin/${itemName}/index/`}
				  />)}
				</div>
			);
		}
	}

	ListComponent.propTypes = {
		match: PropTypes.object,
		items: PropTypes.array.isRequired,
		pages: PropTypes.array.isRequired,
		deletedItemId: PropTypes.string.isRequired,
		changedItem: PropTypes.object.isRequired,
		fetchItems: PropTypes.func.isRequired,
		deleteItem: PropTypes.func.isRequired,
		changeItemStatus: PropTypes.func
	};

	return ListComponent;
}