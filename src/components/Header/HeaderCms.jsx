import React from 'react';
import {NavLink} from 'react-router-dom';
import SearchForm from './SearchForm';

class HeaderCms extends React.Component {
	render() {
		return (
			<header className="container-fluid mb-3 shadow">
				<nav className="navbar navbar-expand-sm border navbar-light bg-primary row">
				  <a className="navbar-brand" href="#1">My News Site</a>
				  <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarMenu" aria-controls="navbarMenu" aria-expanded="false" aria-label="Toggle navigation">
				    <i className="navbar-toggler-icon"></i>
				  </button>
				  <div className="collapse navbar-collapse" id="navbarMenu">
				    <ul className="navbar-nav mr-auto">
				      <li className="nav-item active">
				        <NavLink className="nav-link text-white" to="/">Home</NavLink>
				      </li>
				      <li className="nav-item">
				        <NavLink className="nav-link text-white" to="/admin/article/index">Article</NavLink>
				      </li>
				      <li className="nav-item">
				        <NavLink className="nav-link text-white" to="/admin/category/index">Category</NavLink>
				      </li>
				      <li className="nav-item">
				        <NavLink className="nav-link text-white" to="/admin/tag/index">Tag</NavLink>
				      </li>
				      <li className="nav-item">
				        <NavLink className="nav-link text-white" to="/admin/comment/index">Comment</NavLink>
				      </li>
				      <li className="nav-item">
				        <NavLink className="nav-link text-white" to="/admin/user/index">User</NavLink>
				      </li>
				    </ul>
				    <SearchForm />
				  </div>
				</nav>
			</header>
		);
	}
}

export default HeaderCms;