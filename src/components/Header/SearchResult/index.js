import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import FindedArticles from './FindedArticles';
import FindedComments from './FindedComments';
import FindedUsers from './FindedUsers';
import {sendQuery} from '../../../actions/SearchActions';

class SearchResult extends React.Component {

	componentDidMount() {
		const {keyword} = this.props.match.params;
		const {sendQueryAction} = this.props;
		sendQueryAction(keyword);
	}

	componentDidUpdate(prevProps, prevState, prevContext) {
		const {sendQueryAction} = this.props;
		const {keyword} = this.props.match.params;
		const {keyword: keywordPrev} = prevProps.match.params;
		if (keyword !== keywordPrev)
			sendQueryAction(keyword);
	}

	showArticles() {
		const {articles} = this.props.result;
		return (typeof articles !== 'undefined' && articles.length > 0)
			&& (<FindedArticles articles={articles}/>);
	}

	showUsers() {
		const {users} = this.props.result;
		return (typeof users !== 'undefined' && users.length > 0)
			&& (<FindedUsers users={users}/>);
	}

	showComments() {
		const {comments} = this.props.result;
		return (typeof comments !== 'undefined' && comments.length > 0)
			&& (<FindedComments comments={comments}/>);
	}

	showHeader() {
		const {articles, users, comments} = this.props.result;
		let header = ((typeof articles !== 'undefined' && articles.length > 0)
			|| (typeof users !== 'undefined' && users.length > 0)
				|| (typeof comments !== 'undefined' && comments.length > 0))
					? 'Results' : 'No results.';
		return (
			<h4 className="list-group-item active mb-2">{header}</h4>
		);
	}

	render() {
		return (
			<div className="col-md-7 col-lg-8 articles">
				{this.showHeader()}
				{this.showArticles()}
				{this.showUsers()}
				{this.showComments()}
			</div>
		);
	}
}

const mapStateToProps = store => {
  const {result} = store.search;
  return {
    result: result
  }
};

const mapDispatchToProps = dispatch => ({
  sendQueryAction: keyword => dispatch(sendQuery(keyword))
});

SearchResult.propTypes = {
	match: PropTypes.object.isRequired,
	result: PropTypes.object.isRequired,
	sendQueryAction: PropTypes.func.isRequired
};

export default connect(
	mapStateToProps,
  mapDispatchToProps
)(SearchResult);