import React from 'react';
import PropTypes from 'prop-types';
import {NavLink} from 'react-router-dom';

class FindedComments extends React.Component {

	getComments() {
		const {comments} = this.props;
		return comments.map(comment => (
			<NavLink className="list-group-item list-group-item-action" to={`/single/${comment.article.id}`} key={comment.id}>
				<div>
					{comment.text.slice(0, 150)+'...'}
				</div>
				<div className="d-flex justify-content-between">
					<small>
						By {comment.user.name}.									
					</small>
					<small>
						{comment.article.title}
					</small>
				</div>
			</NavLink>
		));
	}

	render() {
		return (
			<div className="list-group shadow mb-3">
				<h4 className="list-group-item active">
					Comments:
				</h4>
				{this.getComments()}
			</div>
		);
	}
}

FindedComments.propTypes = {
	comments: PropTypes.array.isRequired
};

export default FindedComments;