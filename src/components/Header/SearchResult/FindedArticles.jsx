import React from 'react';
import PropTypes from 'prop-types';
import {NavLink} from 'react-router-dom';

class FindedArticles extends React.Component {

	getArticles() {
		const {articles} = this.props;
		return articles.map(article => (
			<NavLink className="list-group-item list-group-item-action" to={`/single/${article.id}`} key={article.id}>
				<h5>
					{article.title}
				</h5>
				<p>
					{article.content.slice(0, 150)+'...'}
				</p>
				<div className="d-flex justify-content-between">
					<small>
						By {article.user.name}.									
					</small>
					<small>
						{article.last_update}
					</small>
				</div>
			</NavLink>
		));
	}

	render() {
		return (
			<div className="list-group shadow mb-3">
				<h4 className="list-group-item active">
					Articles:
				</h4>
				{this.getArticles()}
			</div>
		);
	}
}

FindedArticles.propTypes = {
	articles: PropTypes.array.isRequired
};

export default FindedArticles;