import React from 'react';
import {NavLink} from 'react-router-dom';
import SearchForm from './SearchForm';

class Header extends React.Component {
	render() {
		return (
			<header className="container-fluid mb-3 shadow">
				<nav className="navbar navbar-expand-sm border navbar-light bg-primary row">
				  <a className="navbar-brand" href="#1">My News Site</a>
				  <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarMenu" aria-controls="navbarMenu" aria-expanded="false" aria-label="Toggle navigation">
				    <i className="navbar-toggler-icon"></i>
				  </button>
				  <div className="collapse navbar-collapse" id="navbarMenu">
				    <ul className="navbar-nav mr-auto">
				      <li className="nav-item active">
				        <NavLink className="nav-link text-white" to="/">Home</NavLink>
				      </li>
				      <li className="nav-item dropdown">
				        <a className="nav-link text-white dropdown-toggle" href="#1" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				          Categories
				        </a>
				        <div className="dropdown-menu" aria-labelledby="navbarDropdown">
				          <a className="dropdown-item" href="#1">Plitics</a>
				          <div className="dropdown-divider"></div>
				          <a className="dropdown-item" href="#1">Sport</a>
				          <div className="dropdown-divider"></div>
				          <a className="dropdown-item" href="#1">Something</a>
				        </div>
				      </li>
				      <li className="nav-item">
				        <NavLink className="nav-link text-white" to="/about">About</NavLink>
				      </li>
				      <li className="nav-item">
				        <NavLink className="nav-link text-white" to="/admin">Admin</NavLink>
				      </li>
				    </ul>
				    <SearchForm />
				  </div>
				</nav>
			</header>
		);
	}
}

export default Header;