import React from 'react';

class Footer extends React.Component {
	render() {
		return (
			<footer className="container-fluid">
				<div className="row justify-content-center bg-secondary text-white text-center">
					<ul className="col-md-4 list-group list-group-flush">
					  <li className="list-group-item bg-secondary">Contact us:</li>
					  <li className="list-group-item bg-secondary">
					  	<a href="mailto:#" className="text-white">mysite@test.com</a>
					  </li>
					  <li className="list-group-item bg-secondary">
					  	<a href="tel:#" className="text-white">000-000-000</a>
						</li>
					</ul>
					<ul className="col-md-4 list-group list-group-flush">
					  <li className="list-group-item bg-secondary">Friends:</li>
					  <li className="list-group-item bg-secondary">
					  	<a href="#1" className="text-white">www.somesite.com</a>
					  </li>
					  <li className="list-group-item bg-secondary">
					  	<a href="#1" className="text-white">www.anothersite.com</a>
					  </li>
					</ul>
				</div>
			</footer>
		);
	}
}

export default Footer;