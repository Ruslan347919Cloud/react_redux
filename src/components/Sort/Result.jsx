import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';

class Result extends React.Component {
	render() {
		const {
			id,
			title,
			content,
			last_update,
			image
		} = this.props.article;
		return (
			<div className="card border-primary shadow mb-3">
			  <Link className="row no-gutters card-link text-dark" to={`/single/${id}`}>
			    <div className="col-md-4 align-self-center p-1">
			      <img className="card-img" src={window.location.origin+'/uploads/'+image} alt=""/>
			    </div>
			    <div className="col-md-8">
			      <div className="card-body">
			        <h5 className="card-title">{title}</h5>
			        <p className="card-text">
			        	{content.slice(0, 100)+'...'}
			      	</p>
			        <p className="card-text">
			        	<small className="text-muted">
			        		Last updated {last_update}
			        	</small>
		        	</p>
			      </div>
			    </div>
			  </Link>
			</div>
		);
	}
}

Result.propTypes = {
	article: PropTypes.object.isRequired
};

export default Result;