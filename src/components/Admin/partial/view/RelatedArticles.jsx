import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';

const RelatedArticles = props => {
	let {articles} = props;
	let articlesList = (typeof articles !== 'undefined')
		? (articles.map(article => (
			<Link className="list-group-item list-group-item-action d-flex justify-content-between border"
				to={`/single/${article.id}`} key={article.id}
			>
		    <span>{article.title}</span>
		    <span>Last update: {article.last_update}</span>
		  </Link>
		))) : null;
	return (
		<React.Fragment>
			<h3 className="mt-4">
				{(articles.length > 0) ? 'Related articles' : 'No articles'}
			</h3>
			<div className="list-group mb-3 shadow">
				{articlesList}
			</div>
		</React.Fragment>
	);
}

RelatedArticles.propTypes = {
	articles: PropTypes.array.isRequired
};

export default RelatedArticles;