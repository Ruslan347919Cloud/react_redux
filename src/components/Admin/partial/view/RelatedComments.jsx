import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';

const RelatedComments = props => {
	let {comments} = props;
	let commentsList = (typeof comments !== 'undefined')
		? (comments.map(comment => (
			<Link className="list-group-item list-group-item-action d-flex justify-content-between border"
				to={`/admin/comment/view/${comment.id}`} key={comment.id}
			>
		    <span>{comment.text}</span>
		    <div>
			    <span className="pr-2">by {comment.user.name}.</span>
			    <span>Added: {comment.put_date}</span>
		    </div>
		  </Link>
		))) : null;
	return (
		<React.Fragment>
			<h3 className="mt-4">
				{(comments.length > 0) ? 'Related comments' : 'No comments'}
			</h3>
			<div className="list-group mb-3 shadow">
				{commentsList}
			</div>
		</React.Fragment>
	);
}

RelatedComments.propTypes = {
	comments: PropTypes.array.isRequired
};

export default RelatedComments;