import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';
import RelatedArticles from '../../partial/view/RelatedArticles';

class SingleCategory extends React.Component {

	deleteCategory = event => {
		event.preventDefault();
		const {id} = this.props.item;
		this.props.deleteItem(id, 'category');
	}

	render() {
		const {item, junctionItems} = this.props;
		return (
			<React.Fragment>
				<h2>{item.title}</h2>
				<div className="list-group mb-3 shadow">
					<div className="list-group-item d-flex justify-content-between border">
				    <span>Id</span>
				    <span>{item.id}</span>
				  </div>
					<div className="list-group-item d-flex justify-content-between border">
				    <span>Title</span>
				    <span>{item.title}</span>
				  </div>
				</div>
				<Link className="btn btn-primary btn mr-2" to={`/admin/category/form/${item.id}`}>
					Update category info
				</Link>
				<button className="btn btn-danger btn mr-2" type="button" onClick={this.deleteCategory}>
					Delete category
				</button>
				<RelatedArticles articles={junctionItems} />
			</React.Fragment>
		);
	}
}

SingleCategory.propTypes = {
	item: PropTypes.object.isRequired,
	junctionItems: PropTypes.array.isRequired,
	deleteItem: PropTypes.func.isRequired
};

export default SingleCategory;