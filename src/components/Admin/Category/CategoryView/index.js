import {connect} from 'react-redux';
import {compose} from 'redux';
import SingleCategory from './SingleCategory';
import {withView} from '../../../hoc/withView';
import {
  fetchSingleCategory
} from '../../../../actions/admin/Category/CategoryViewActions';
import {
  deleteItem
} from '../../../../actions/admin/ListActions';

const mapStateToProps = store => {
  const {category, articles, pages} = store.categoryView;
  const {deletedItemId} = store.list;
  return {
    itemName: 'category',
    item: category,
    junctionItems: articles,
    pages: pages,
    deletedItemId: deletedItemId
  }
};

const mapDispatchToProps = dispatch => ({
  fetchSingleItem: (id, page, limit) => dispatch(
    fetchSingleCategory(id, page, limit)),
  deleteItem: (id, controller) => dispatch(deleteItem(id, controller))
});

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  withView
)(SingleCategory);