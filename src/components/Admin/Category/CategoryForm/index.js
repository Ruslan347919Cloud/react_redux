import {connect} from 'react-redux';
import {compose} from 'redux';
import CategoryFormContent from './CategoryFormContent';
import {withForm} from '../../../hoc/withForm';
import {
  findItem,
  createItem,
  updateItem,
  clearForm
} from '../../../../actions/admin/FormActions';

const mapStateToProps = store => {
  const {
  	currentCategoryId,
  	currentCategoryTitle,
  	newCategoryData
  } = store.categoryForm;
  
  return {
    itemName: 'category',
  	updatingItemId: currentCategoryId,
    newItemData: newCategoryData,
    initialValues: {
    	title: currentCategoryTitle
    }
  }
};

const mapDispatchToProps = dispatch => ({
	findItem:
    (id, controller) => dispatch(findItem(id, controller)),
  createItem:
    (data, controller) => dispatch(createItem(data, controller)),
  updateItem:
    (data, controller) => dispatch(updateItem(data, controller)),
  clearForm: () => dispatch(clearForm())
});

export default compose(
	connect(mapStateToProps, mapDispatchToProps),
	withForm
)(CategoryFormContent);