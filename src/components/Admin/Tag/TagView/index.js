import {connect} from 'react-redux';
import {compose} from 'redux';
import SingleTag from './SingleTag';
import {withView} from '../../../hoc/withView';
import {
  fetchSingleTag
} from '../../../../actions/admin/Tag/TagViewActions';
import {
  deleteItem
} from '../../../../actions/admin/ListActions';

const mapStateToProps = store => {
  const {tag, articles, pages} = store.tagView;
  const {deletedItemId} = store.list;
  return {
    itemName: 'tag',
    item: tag,
    junctionItems: articles,
    pages: pages,
    deletedItemId: deletedItemId
  }
};

const mapDispatchToProps = dispatch => ({
  fetchSingleItem: (id, page, limit) => dispatch(
    fetchSingleTag(id, page, limit)),
  deleteItem: (id, controller) => dispatch(deleteItem(id, controller))
});

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  withView
)(SingleTag);