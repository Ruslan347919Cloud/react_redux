import React from 'react';
import {reduxForm, Field} from 'redux-form';
import {
	required,
	maxLength,
	minLength
} from '../../../../utils/validators';
import {Input} from '../../../../common/formControl/';

const maxLength20 = maxLength(20);
const minLength4 = minLength(4);

const TagFormContent = props => {
	const {handleSubmit, pristine, invalid, submitting} = props;
	return (
		<form onSubmit={handleSubmit}>
			<div className="form-group">
		    <label htmlFor="title">Title</label>
		    <Field
		    	name="title"
		    	type="text"
		    	className="form-control"
		    	id="title"
		    	placeholder="Enter tag title"
		    	component={Input}
		    	validate={[required, minLength4, maxLength20]}
	    	/>
		  </div>
		  <button
		  	type="submit"
		  	className="btn btn-primary"
		  	disabled={submitting || pristine || invalid}
	  	>
		  	Submit
	  	</button>
		</form>
	);
}

export default reduxForm({
  form: 'tagForm',
	enableReinitialize: true,
  onSubmitSuccess: (result, dispatch, props) => {
    props.reset();
  }
})(TagFormContent);