import {connect} from 'react-redux';
import {compose} from 'redux';
import TagFormContent from './TagFormContent';
import {withForm} from '../../../hoc/withForm';
import {
  findItem,
  createItem,
  updateItem,
  clearForm
} from '../../../../actions/admin/FormActions';

const mapStateToProps = store => {
  const {
  	currentTagId,
  	currentTagTitle,
  	newTagData
  } = store.tagForm;
  
  return {
    itemName: 'tag',
  	updatingItemId: currentTagId,
    newItemData: newTagData,
    initialValues: {
    	title: currentTagTitle
    }
  }
};

const mapDispatchToProps = dispatch => ({
	findItem:
    (id, controller) => dispatch(findItem(id, controller)),
  createItem:
    (data, controller) => dispatch(createItem(data, controller)),
  updateItem:
    (data, controller) => dispatch(updateItem(data, controller)),
  clearForm: () => dispatch(clearForm())
});

export default compose(
	connect(mapStateToProps, mapDispatchToProps),
	withForm
)(TagFormContent);