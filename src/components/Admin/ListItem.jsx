import React from 'react';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';

class ListItem extends React.Component {

	deleteRecord = event => {
		event.preventDefault();
		const {itemName, deleteItem} = this.props;
		const {value} = event.target.querySelector('input');
		deleteItem(value, itemName);
	}

	changeVisibleStatus = event => {
		event.preventDefault();
		const {
			itemName,
			changeItemStatus
		} = this.props;
		const {id} = this.props.item;
		changeItemStatus(id, itemName);
	}

	changeStatusButton() {
		const {id, visible} = this.props.item;
		let icon = (Number(visible))
			? (<span>disallow<i className="fa fa-ban" aria-hidden="true"></i></span>)
				: (<span>allow<i className="fa fa-check-circle" aria-hidden="true"></i></span>);
		return (
			<a href="#1" onClick={this.changeVisibleStatus}>
    		<input type="text" defaultValue={id} hidden={true} />
    		{icon}
    	</a>
		);
	}

	getValues() {
		const {item} = this.props;
		const values = Object.values(item);
		return values.map(value => (
			<td key={Math.random()}>
				{(value !== null) ? (value.slice(0, 15)) : ''}
			</td>
		));
	}

	render() {
		const {id, visible} = this.props.item;
		const {itemName} = this.props;
		return (
			<tbody>
				<tr>
			    {this.getValues()}
			    <td>
			    	{(typeof visible !== 'undefined') && this.changeStatusButton()}
			    	<Link to={`/admin/${itemName}/view/${id}`}>
			    		view
			    		<i className="fa fa-eye" aria-hidden="true"></i>
		    		</Link>
		    		<Link to={`/admin/${itemName}/form/${id}`}>
		    			edit
			    		<i className="fa fa-pencil" aria-hidden="true"></i>
			    	</Link>
			    	<a href="#1" onClick={this.deleteRecord}>
			    		del
			    		<input type="text" defaultValue={id} hidden={true} />
			    		<i className="fa fa-trash-o" aria-hidden="true"></i>
			    	</a>
			    </td>
		    </tr>
		  </tbody>
		);
	}
}

ListItem.propTypes = {
	item: PropTypes.object.isRequired,
	itemName: PropTypes.string.isRequired,
	deleteItem: PropTypes.func.isRequired,
	changeItemStatus: PropTypes.func.isRequired,
};

export default ListItem;