import {connect} from 'react-redux';
import {compose} from 'redux';
import CommentFormContent from './CommentFormContent';
import {withForm} from '../../../hoc/withForm';
import {
  findItem,
  createItem,
  updateItem,
  clearForm
} from '../../../../actions/admin/FormActions';

const mapStateToProps = store => {
  const {
  	currentCommentId,
    currentCommentText,
    currentCommentUserId,
    currentCommentArticleId,
    newCommentData
  } = store.commentForm;
  
  return {
    itemName: 'comment',
  	updatingItemId: currentCommentId,
    newItemData: newCommentData,
    initialValues: {
    	text: currentCommentText,
      user_id: currentCommentUserId,
      article_id: currentCommentArticleId
    }
  }
};

const mapDispatchToProps = dispatch => ({
	findItem:
    (id, controller) => dispatch(findItem(id, controller)),
  createItem:
    (data, controller) => dispatch(createItem(data, controller)),
  updateItem:
    (data, controller) => dispatch(updateItem(data, controller)),
  clearForm: () => dispatch(clearForm())
});

export default compose(
	connect(mapStateToProps, mapDispatchToProps),
	withForm
)(CommentFormContent);