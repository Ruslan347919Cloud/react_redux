import React from 'react';
import {reduxForm, Field} from 'redux-form';
import {
	required,
	maxLength,
	minLength
} from '../../../../utils/validators';
import {Input, TextArea} from '../../../../common/formControl/';

const maxLength300 = maxLength(300);
const minLength4 = minLength(4);

const CommentFormContent = props => {
	const {handleSubmit, pristine, invalid, submitting} = props;
	return (
		<form onSubmit={handleSubmit}>
			<div className="form-group">
		    <label htmlFor="text">Comment text</label>
		    <Field
		    	name="text"
		    	className="form-control"
		    	id="text"
		    	rows="5"
		    	placeholder="Enter comment text"
		    	component={TextArea}
		    	validate={[required, minLength4, maxLength300]}
	    	/>
		  </div>
		  <div className="form-group">
		    <label htmlFor="user_id">User ID</label>
		    <Field
		    	name="user_id"
		    	type="text"
		    	className="form-control"
		    	id="user_id"
		    	component={Input}
		    	validate={[required]}
	    	/>
		  </div>
		  <div className="form-group">
		    <label htmlFor="article_id">Article ID</label>
		    <Field
		    	name="article_id"
		    	type="text"
		    	className="form-control"
		    	id="article_id"
		    	component={Input}
		    	validate={[required]}
	    	/>
		  </div>
		  <button
		  	type="submit"
		  	className="btn btn-primary"
		  	disabled={submitting || pristine || invalid}
	  	>
		  	Submit
	  	</button>
		</form>
	);
}

export default reduxForm({
  form: 'commentForm',
	enableReinitialize: true,
  onSubmitSuccess: (result, dispatch, props) => {
    props.reset();
  }
})(CommentFormContent);