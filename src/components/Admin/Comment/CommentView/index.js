import {connect} from 'react-redux';
import {compose} from 'redux';
import SingleComment from './SingleComment';
import {withView} from '../../../hoc/withView';
import {
  fetchSingleComment,
  changeCommentStatus
} from '../../../../actions/admin/Comment/CommentViewActions';
import {
  deleteItem
} from '../../../../actions/admin/ListActions';

const mapStateToProps = store => {
  const {comment, status} = store.commentView;
  const {deletedItemId} = store.list;
  return {
    itemName: 'comment',
    item: comment,
    deletedItemId: deletedItemId,
    status: status
  }
};

const mapDispatchToProps = dispatch => ({
  fetchSingleItem: (id, page, limit) => dispatch(
    fetchSingleComment(id, page, limit)),
  deleteItem: (id, controller) => dispatch(deleteItem(id, controller)),
  changeStatus: (id) => dispatch(changeCommentStatus(id))
});

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  withView
)(SingleComment);