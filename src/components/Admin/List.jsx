import {connect} from 'react-redux';
import {compose} from 'redux';
import ListItem from './ListItem';
import {withList} from '../hoc/withList';
import {
  fetchItems,
  deleteItem,
  changeItemStatus
} from '../../actions/admin/ListActions';

const mapStateToProps = store => {
  const {items, pages, deletedItemId, changedItem} = store.list;
  return {
    items: items,
    pages: pages,
    deletedItemId: deletedItemId,
    changedItem: changedItem
  }
};

const mapDispatchToProps = dispatch => ({
  fetchItems:
  	(page, limit, controller) => dispatch(fetchItems(page, limit, controller)),
	deleteItem:
    (id, controller) => dispatch(deleteItem(id, controller)),
  changeItemStatus: (id, controller) => dispatch(
    changeItemStatus(id, controller))
});

export default compose(
	connect(mapStateToProps, mapDispatchToProps),
	withList
)(ListItem);