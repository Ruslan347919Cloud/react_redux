import React from 'react';
import {reduxForm, Field} from 'redux-form';
import {
	required,
	maxLength,
	minLength,
	email
} from '../../../../utils/validators';
import {Input} from '../../../../common/formControl/';

const maxLength20 = maxLength(20);
const minLength4 = minLength(4);

const UserFormContent = props => {
	const {handleSubmit, pristine, invalid, submitting} = props;
	return (
		<form onSubmit={handleSubmit}>
			<div className="form-group">
		    <label htmlFor="name">Name</label>
		    <Field
		    	name="name"
		    	type="text"
		    	className="form-control"
		    	id="name"
		    	placeholder="Enter your name"
		    	component={Input}
		    	validate={[required, minLength4, maxLength20]}
	    	/>
		  </div>
		  <div className="form-group">
		    <label htmlFor="email">Email address</label>
		    <Field
		    	name="email"
		    	type="email"
		    	className="form-control"
		    	id="email"
		    	placeholder="Enter your e-mail"
		    	aria-describedby="emailHelp"
		    	component={Input}
		    	validate={[required, email]}
	    	/>
		    <small id="emailHelp" className="form-text text-muted">
		    	We will never share your email with anyone else.
		    </small>
		  </div>
		  <div className="form-group">
		    <label htmlFor="password">Password</label>
		    <Field
		    	name="password"
		    	type="password"
		    	className="form-control"
		    	id="password"
		    	placeholder="Enter password"
		    	component={Input}
		    	validate={[required, minLength4, maxLength20]}
	    	/>
		  </div>
		  <button
		  	type="submit"
		  	className="btn btn-primary"
		  	disabled={submitting || pristine || invalid}
	  	>
		  	Submit
	  	</button>
		</form>
	);
}

export default reduxForm({
  form: 'userForm',
  enableReinitialize: true,
  onSubmitSuccess: (result, dispatch, props) => {
    props.reset();
  }
})(UserFormContent);