import {connect} from 'react-redux';
import {compose} from 'redux';
import UserFormContent from './UserFormContent';
import {withForm} from '../../../hoc/withForm';
import {
	findItem,
	createItem,
	updateItem,
	clearForm
} from '../../../../actions/admin/FormActions';

const mapStateToProps = store => {
  const {
  	currentUserId,
  	currentUserName,
  	currentUserEmail,
  	currentUserPassword,
  	newUserData
  } = store.userForm;
  
  return {
  	itemName: 'user',
  	updatingItemId: currentUserId,
    newItemData: newUserData,
    initialValues: {
    	name: currentUserName,
    	email: currentUserEmail,
    	password: currentUserPassword,
    }
  }
};

const mapDispatchToProps = dispatch => ({
	findItem:
		(id, controller) => dispatch(findItem(id, controller)),
  createItem:
  	(data, controller) => dispatch(createItem(data, controller)),
  updateItem:
  	(data, controller) => dispatch(updateItem(data, controller)),
  clearForm: () => dispatch(clearForm())
});

export default compose(
	connect(mapStateToProps, mapDispatchToProps),
	withForm
)(UserFormContent);