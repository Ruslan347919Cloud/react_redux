import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';
import RelatedArticles from '../../partial/view/RelatedArticles';

class SingleUser extends React.Component {

	deleteUser = event => {
		event.preventDefault();
		const {id} = this.props.item;
		this.props.deleteItem(id, 'user');
	}

	render() {
		const {item, junctionItems} = this.props;
		return (
			<React.Fragment>
				<h2>{item.name}</h2>
				<div className="list-group mb-3 shadow">
					<div className="list-group-item d-flex justify-content-between border">
				    <span>Photo</span>
				    <span>{item.photo}</span>
				  </div>
					<div className="list-group-item d-flex justify-content-between border">
				    <span>Id</span>
				    <span>{item.id}</span>
				  </div>
				  <div className="list-group-item d-flex justify-content-between border">
				    <span>Name</span>
				    <span>{item.name}</span>
				  </div>
				  <div className="list-group-item d-flex justify-content-between border">
				    <span>Email</span>
				    <span>{item.email}</span>
				  </div>
				  <div className="list-group-item d-flex justify-content-between border">
				    <span>Password</span>
				    <span>{item.password}</span>
				  </div>
				</div>
				<button className="btn btn-primary btn mr-2" type="button">
					Create article
				</button>
				<Link className="btn btn-primary btn mr-2" to={`/admin/user/form/${item.id}`}>
					Update user info
				</Link>
				<button className="btn btn-primary btn mr-2" type="button">
					Upload user photo
				</button>
				<button className="btn btn-danger btn mr-2" type="button" onClick={this.deleteUser}>
					Delete user
				</button>
				<RelatedArticles articles={junctionItems} />
			</React.Fragment>
		);
	}
}

SingleUser.propTypes = {
	item: PropTypes.object.isRequired,
	junctionItems: PropTypes.array.isRequired,
	deleteItem: PropTypes.func.isRequired
};

export default SingleUser;