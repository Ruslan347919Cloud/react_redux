import {connect} from 'react-redux';
import {compose} from 'redux';
import SingleUser from './SingleUser';
import {withView} from '../../../hoc/withView';
//import {Link, Redirect} from 'react-router-dom';
//import Pagination from '../../../Pagination/';
import {
	fetchSingleUser
} from '../../../../actions/admin/User/UserViewActions';
import {
	deleteItem
} from '../../../../actions/admin/ListActions';

/*

class UserView extends React.Component {

	constructor(props) {
		super(props);
		this.pageSize = 3;
	}

	componentDidMount() {
		const {id} = this.props.match.params;
		const {fetchSingleUser} = this.props;
		fetchSingleUser(id, this.getCurrentPage(), this.pageSize);
	}

	componentDidUpdate(prevProps) {
		const {fetchSingleUser} = this.props;
		const {id, page} = this.props.match.params;
		const {id: idPrev, page: pagePrev} = prevProps.match.params;
		if (id !== idPrev || page !== pagePrev)
			fetchSingleUser(id, this.getCurrentPage(), this.pageSize);
	}

	deleteUser = event => {
		event.preventDefault();
		const {id} = this.props.match.params;
		this.props.deleteItem(id, 'user');
	}

	getUserArticles() {
		let {articles} = this.props;
		let articlesList = (typeof articles !== 'undefined')
			? (articles.map(article => (
				<Link className="list-group-item list-group-item-action d-flex justify-content-between border"
					to={`/single/${article.id}`} key={article.id}
				>
			    <span>{article.title}</span>
			    <span>Last update: {article.last_update}</span>
			  </Link>
			))) : null;
		return (
			<React.Fragment>
				<h3 className="mt-4">
					{(articles.length > 0) ? 'My articles' : 'No articles'}
				</h3>
				<div className="list-group mb-3 shadow">
					{articlesList}
				</div>
			</React.Fragment>
		);
	}

	getCurrentPage() {
		return (typeof this.props.match.params.page !== 'undefined')
			? Number(this.props.match.params.page) : 1;
	}

	render() {
		const {user, pages, deletedItemId} = this.props;
		const {id} = this.props.match.params;
		if (deletedItemId !== '')
			return <Redirect to={`/admin/user/index/`}/>;
		return (
			<div className="col-md-7 col-lg-8 articles">
				<h2>{user.name}</h2>
				<div className="list-group mb-3 shadow">
					<div className="list-group-item d-flex justify-content-between border">
				    <span>Photo</span>
				    <span>{user.photo}</span>
				  </div>
					<div className="list-group-item d-flex justify-content-between border">
				    <span>Id</span>
				    <span>{user.id}</span>
				  </div>
				  <div className="list-group-item d-flex justify-content-between border">
				    <span>Name</span>
				    <span>{user.name}</span>
				  </div>
				  <div className="list-group-item d-flex justify-content-between border">
				    <span>Email</span>
				    <span>{user.email}</span>
				  </div>
				  <div className="list-group-item d-flex justify-content-between border">
				    <span>Password</span>
				    <span>{user.password}</span>
				  </div>
				</div>
				<button className="btn btn-primary btn mr-2" type="button">
					Create article
				</button>
				<Link className="btn btn-primary btn mr-2" to={`/admin/user/form/${user.id}`}>
					Update user info
				</Link>
				<button className="btn btn-primary btn mr-2" type="button">
					Upload user photo
				</button>
				<button className="btn btn-danger btn mr-2" type="button" onClick={this.deleteUser}>
					Delete user
				</button>
				{this.getUserArticles()}
				{(pages.length > 1) && (<Pagination
					route={'/admin/user/view/'}
					pages={pages}
					componentId={Number(id)}
					currentPage={this.getCurrentPage()}
				/>)}
			</div>
		);
	}
}

UserView.propTypes = {
	match: PropTypes.object.isRequired,
	user: PropTypes.object.isRequired,
	articles: PropTypes.array.isRequired,
	pages: PropTypes.array.isRequired,
	deletedItemId: PropTypes.string.isRequired,
	fetchSingleUser: PropTypes.func.isRequired,
	deleteItem: PropTypes.func.isRequired
};

*/

const mapStateToProps = store => {
  const {user, articles, pages} = store.userView;
  const {deletedItemId} = store.list;
  return {
  	itemName: 'user',
    item: user,
    junctionItems: articles,
    pages: pages,
    deletedItemId: deletedItemId
  }
};

const mapDispatchToProps = dispatch => ({
  fetchSingleItem: (id, page, limit) => dispatch(
  	fetchSingleUser(id, page, limit)),
  deleteItem: (id, controller) => dispatch(deleteItem(id, controller))
});

export default compose(
	connect(mapStateToProps, mapDispatchToProps),
	withView
)(SingleUser);

/*export default connect(
	mapStateToProps,
  mapDispatchToProps
)(UserView);*/