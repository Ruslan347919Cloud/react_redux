import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';
import RelatedComments from '../../partial/view/RelatedComments';

class SingleArticle extends React.Component {

	deleteArticle = event => {
		event.preventDefault();
		const {id} = this.props.item;
		this.props.deleteItem(id, 'article');
	}

	changeVisibleStatus = event => {
		event.preventDefault();
		const {id} = this.props.item;
		const {changeStatus} = this.props;
		changeStatus(id);
	}

	/*setArticleCategories = event => {
		event.preventDefault();
		const {id} = this.props.item;
		const {setCategories} = this.props;
		setCategories(id);
	}

	setArticleTags = event => {
		event.preventDefault();
		const {id} = this.props.item;
		const {setTags} = this.props;
		setTags(id);
	}*/

	render() {
		const {item, junctionItems} = this.props;
		return (
			<React.Fragment>
				<h2>{item.title}</h2>
				<div className="list-group mb-3 shadow">
					<div className="list-group-item border">
					   {item.content}
				  </div>
					<div className="list-group-item d-flex justify-content-between border">
				    <span>Id</span>
				    <span>{item.id}</span>
				  </div>
					<div className="list-group-item d-flex justify-content-between border">
				    <span>Title</span>
				    <span>{item.title}</span>
				  </div>
				  <div className="list-group-item d-flex justify-content-between border">
				    <span>Description</span>
				    <span>{item.description}</span>
				  </div>
				  <div className="list-group-item d-flex justify-content-between border">
				    <span>Last update</span>
				    <span>{item.last_update}</span>
				  </div>
				  <div className="list-group-item d-flex justify-content-between border">
				    <span>Image</span>
				    <span>{item.image}</span>
				  </div>
				  <div className="list-group-item d-flex justify-content-between border">
				    <span>Viewed</span>
				    <span>{item.viewed}</span>
				  </div>
				  <div className="list-group-item d-flex justify-content-between border">
				    <span>Visible</span>
				    <span>{item.visible}</span>
				  </div>
				</div>
				<Link className="btn btn-primary btn mr-2" to={`/admin/article/form/${item.id}`}>
					Update article
				</Link>
				<button className="btn btn-primary btn mr-2" type="button">
					Set image
				</button>
				{/*<Link className="btn btn-primary btn mr-2" to={{
					pathname: `/admin/article/set/category/${item.id}`,
					setAttribute: this.props.setCategories
				}}>
					Set categories
				</Link>
				<Link className="btn btn-primary btn mr-2" to={{
					pathname:`/admin/article/set/tag/${item.id}`,
					setAttribute: this.props.setTags
				}}>
					Set tags
				</Link>*/}
				<Link className="btn btn-primary btn mr-2" to={
					`/admin/article/set/category/${item.id}`
				}>
					Set categories
				</Link>
				<Link className="btn btn-primary btn mr-2" to={
					`/admin/article/set/tag/${item.id}`
				}>
					Set tags
				</Link>
				<button className="btn btn-primary btn mr-2" type="button" onClick={this.changeVisibleStatus}>
					Change status
				</button>
				<button className="btn btn-danger btn mr-2" type="button" onClick={this.deleteArticle}>
					Delete article
				</button>
				<RelatedComments comments={junctionItems}/>
			</React.Fragment>
		);
	}
}

SingleArticle.propTypes = {
	item: PropTypes.object.isRequired,
	junctionItems: PropTypes.array.isRequired,
	deleteItem: PropTypes.func.isRequired,
	changeStatus: PropTypes.func.isRequired
};

export default SingleArticle;