import {connect} from 'react-redux';
import {compose} from 'redux';
import SingleArticle from './SingleArticle';
import {withView} from '../../../hoc/withView';
import {
  fetchSingleArticle,
  changeArticleStatus
} from '../../../../actions/admin/Article/ArticleViewActions';
import {
  deleteItem
} from '../../../../actions/admin/ListActions';

const mapStateToProps = store => {
  const {article, comments, pages, status} = store.articleView;
  const {deletedItemId} = store.list;
  return {
    itemName: 'article',
    item: article,
    junctionItems: comments,
    pages: pages,
    deletedItemId: deletedItemId,
    status: status
  }
};

const mapDispatchToProps = dispatch => ({
  fetchSingleItem: (id, page, limit) => dispatch(
    fetchSingleArticle(id, page, limit)),
  deleteItem: (id, controller) => dispatch(
    deleteItem(id, controller)),
  changeStatus: id => dispatch(changeArticleStatus(id))
});

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  withView
)(SingleArticle);