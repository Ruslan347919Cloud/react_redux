import React from 'react';
import PropTypes from 'prop-types';
import {reduxForm, Field} from 'redux-form';
import {connect} from 'react-redux';
import SetAttributesFormContent from './SetAttributesFormContent';
import {
	setCategories,
	setTags
} from '../../../../actions/admin/Article/SetAttributesActions';

class SetAttributes extends React.Component {

	/*setArticleCategories = event => {
		event.preventDefault();
		const {id} = this.props.item;
		const {setCategories} = this.props;
		setCategories(id);
	}

	setArticleTags = event => {
		event.preventDefault();
		const {id} = this.props.item;
		const {setTags} = this.props;
		setTags(id);
	}*/

	submitHandler = formData => {
		const {setTags, setCategories} = this.props;
		const {attribute, id} = this.props.match.params;
		switch (attribute) {
			case 'category':
				setCategories(id, formData.attributeSelect);
			break;
			case 'tag':
				setTags(id, formData.attributeSelect);
		}
	}

	render() {
		return (
			<div className="col-md-12 articles">
				<SetAttributesFormContent
					onSubmit={this.submitHandler}
				/>
			</div>
		);
	}
}

const mapStateToProps = store => {
  const {categories, tags} = store.setAttributes;
  return {
    categories: categories,
    tags: tags
  }
};

const mapDispatchToProps = dispatch => ({
  setCategories: (id, data) => dispatch(setCategories(id, data)),
  setTags: (id, data) => dispatch(setTags(id, data))
});

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(SetAttributes);