import React from 'react';
import PropTypes from 'prop-types';
import {reduxForm, Field} from 'redux-form';

const SetAttributesFormContent = props => {
	const {
		handleSubmit,
		invalid,
		submitting
	} = props;
	return (
		<form onSubmit={handleSubmit}>
			<div className="form-group">
		    <label htmlFor="attributeSelect">Select categories</label>
		    <Field
		    	className="form-control"
		    	type="select-multiple"
		    	name="attributeSelect"
		    	id="attributeSelect"
		    	component="select"
		    	multiple={true}
	    	>
		      <option>1</option>
		      <option>2</option>
		      <option>3</option>
		      <option>4</option>
		      <option>5</option>
		    </Field>
		  </div>
		  <button
		  	type="submit"
		  	className="btn btn-primary"
		  	disabled={submitting || invalid}
	  	>
		  	Submit
	  	</button>
		</form>
	);
}

export default reduxForm({
  form: 'setAttributesForm',
  enableReinitialize: true,
  onSubmitSuccess: (result, dispatch, props) => {
    props.reset();
  }
})(SetAttributesFormContent);