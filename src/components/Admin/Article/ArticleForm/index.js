import {connect} from 'react-redux';
import {compose} from 'redux';
import ArticleFormContent from './ArticleFormContent';
import {withForm} from '../../../hoc/withForm';
import {
	findItem,
	createItem,
	updateItem,
	clearForm
} from '../../../../actions/admin/FormActions';

const mapStateToProps = store => {
  const {
  	currentArticleId,
    currentArticleTitle,
    currentArticleDescription,
    currentArticleContent,
    currentArticleUserId,
    newArticleData
  } = store.articleForm;
  
  return {
  	itemName: 'article',
  	updatingItemId: currentArticleId,
    newItemData: newArticleData,
    initialValues: {
    	title: currentArticleTitle,
    	description: currentArticleDescription,
    	content: currentArticleContent,
      user_id: currentArticleUserId
    }
  }
};

const mapDispatchToProps = dispatch => ({
	findItem:
		(id, controller) => dispatch(findItem(id, controller)),
  createItem:
  	(data, controller) => dispatch(createItem(data, controller)),
  updateItem:
  	(data, controller) => dispatch(updateItem(data, controller)),
  clearForm: () => dispatch(clearForm())
});

export default compose(
	connect(mapStateToProps, mapDispatchToProps),
	withForm
)(ArticleFormContent);