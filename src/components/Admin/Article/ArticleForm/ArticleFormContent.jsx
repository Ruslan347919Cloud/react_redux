import React from 'react';
import {reduxForm, Field} from 'redux-form';
import {
	required,
	maxLength,
	minLength
} from '../../../../utils/validators';
import {Input, TextArea} from '../../../../common/formControl/';

const maxLength50 = maxLength(50);
const maxLength500 = maxLength(500);
const minLength4 = minLength(4);

const ArticleFormContent = props => {
	const {handleSubmit, pristine, invalid, submitting} = props;
	return (
		<form onSubmit={handleSubmit}>
			<div className="form-group">
		    <label htmlFor="title">Title</label>
		    <Field
		    	name="title"
		    	type="text"
		    	className="form-control"
		    	id="title"
		    	placeholder="Enter title"
		    	component={Input}
		    	validate={[required, minLength4, maxLength50]}
	    	/>
		  </div>
		  <div className="form-group">
		    <label htmlFor="description">Description</label>
		    <Field
		    	name="description"
		    	type="text"
		    	className="form-control"
		    	id="description"
		    	placeholder="Enter description"
		    	component={Input}
		    	validate={[minLength4, maxLength50]}
	    	/>
		  </div>
		  <div className="form-group">
		    <label htmlFor="content">Content</label>
		    <Field
		    	name="content"
		    	className="form-control"
		    	id="content"
		    	placeholder="Enter article text"
		    	rows="10"
		    	component={TextArea}
		    	validate={[required, minLength4, maxLength500]}
	    	/>
		  </div>
		  <div className="form-group">
		    <label htmlFor="user_id">User ID</label>
		    <Field
		    	name="user_id"
		    	type="text"
		    	className="form-control"
		    	id="user_id"
		    	component={Input}
		    	validate={[required]}
	    	/>
		  </div>
		  <button
		  	type="submit"
		  	className="btn btn-primary"
		  	disabled={submitting || pristine || invalid}
	  	>
		  	Submit
	  	</button>
		</form>
	);
}

export default reduxForm({
  form: 'articleForm',
  enableReinitialize: true,
  onSubmitSuccess: (result, dispatch, props) => {
    props.reset();
  }
})(ArticleFormContent);