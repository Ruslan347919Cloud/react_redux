import React from 'react';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';

class Categories extends React.Component {

	getCategories() {
		const {categories} = this.props;
		return categories.map(category => (
			<Link
				className="list-group-item list-group-item-action border d-flex justify-content-between align-items-center"
				to={`/sort/categories/${category.id}`}
				key={category.id}
			>
		    <span>{category.title}</span>
		    <span className="badge badge-primary text-white">
		    	{category.articlesCount}
		    </span>
		  </Link>
		));
	}

	render() {
		return (
			<div className="list-group mb-3 shadow sidebar__categories">
				<h5 className="list-group-item list-group-item-primary active">
					Categories
				</h5>
				{this.getCategories()}
			</div>
		);
	}
}

Categories.propTypes = {
	categories: PropTypes.array.isRequired
};

export default Categories;