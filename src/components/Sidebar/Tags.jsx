import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';

class Tags extends React.Component {

	getTags() {
		const {tags} = this.props;
		return tags.map(tag => (
			<Link to={`/sort/tags/${tag.id}`} className="card-link" key={tag.id}>
				{tag.title}
			</Link>
		));
	}

	render() {
		return (
			<div className="card border-primary shadow">
			  <h5 className="card-header text-white bg-primary">
			    Tags
			  </h5>
			  <div className="card-body">
			  	{this.getTags()}
			  </div>
			</div>
		);
	}
}

Tags.propTypes = {
	tags: PropTypes.array.isRequired
};

export default Tags;