import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';

class Authors extends React.Component {

	getAuthors() {
		const {authors} = this.props;
		return authors.map(author => (
			<Link
				className="list-group-item list-group-item-action border d-flex justify-content-between align-items-center p-2"
				to={`/sort/authors/${author.id}`}
				key={author.id}
			>
				<img src={window.location.origin+'/uploads/'+author.photo} alt="" className="rounded" />
		    <span>{author.name}</span>
		    <span className="badge badge-secondary">
		    	{author.articlesCount}
		    </span>
		  </Link>
		));
	}

	render() {
		return (
			<div className="list-group mb-3 shadow sidebar__authors">
				<h5 className="list-group-item list-group-item-primary active">
					Authors
				</h5>
				{this.getAuthors()}
			  <a href="#1" className="list-group-item list-group-item-action border text-center">
			  	Show all
			  </a>
			</div>
		);
	}
}

Authors.propTypes = {
	authors: PropTypes.array.isRequired
};

export default Authors;