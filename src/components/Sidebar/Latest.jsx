import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';

class Latest extends React.Component {

	getLatest() {
		const {latest} = this.props;
		return latest.map(article => (
			<Link className="list-group-item list-group-item-action border" to={`/single/${article.id}`} key={article.id}>
		    <small>{article.last_update}</small>
		    <p className="mb-1">{article.content.slice(0, 100)+'...'}</p>
		  </Link>
		));
	}

	render() {
		return (
			<div className="list-group mb-3 shadow sidebar__latest">
				<h5 className="list-group-item list-group-item-primary active">
					Latest
				</h5>
			  {this.getLatest()}
			</div>
		);
	}
}

Latest.propTypes = {
	latest: PropTypes.array.isRequired
};

export default Latest;