import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import Latest from './Latest';
import Categories from './Categories';
import Authors from './Authors';
import Tags from './Tags';
import {fetchSidebarData} from '../../actions/SidebarActions';

class Sidebar extends React.Component {

	componentDidMount() {
		this.props.fetchSidebarDataAction();
	}

	render() {
		const {
			latest,
			categories,
			authors,
			tags
		} = this.props;
		return (
			<div className="col-md-5 col-lg-4 sidebar">
				<Latest latest={latest}/>
				<Categories categories={categories}/>
				<Authors authors={authors}/>
				<Tags tags={tags}/>
			</div>
		);
	}
}

const mapStateToProps = store => {
  const {latest, categories, authors, tags} = store.sidebar;
  return {
    latest: latest,
    categories: categories,
    authors: authors,
    tags: tags
  }
};

const mapDispatchToProps = dispatch => ({
  fetchSidebarDataAction: () => dispatch(fetchSidebarData())
});

Sidebar.propTypes = {
	latest: PropTypes.array.isRequired,
	categories: PropTypes.array.isRequired,
	authors: PropTypes.array.isRequired,
	tags: PropTypes.array.isRequired,
	fetchSidebarDataAction: PropTypes.func.isRequired
};

export default connect(
	mapStateToProps,
  mapDispatchToProps
)(Sidebar);