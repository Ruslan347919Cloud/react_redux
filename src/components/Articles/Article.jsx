import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';

class Article extends React.Component {
	render() {
		let {
			id,
			title,
			description,
			content,
			last_update,
			image,
			viewed,
			user: {
				name
			}
		} = this.props.article;
		image = image || 'no_image.jpg';
		return (
			<div className="card mb-3 shadow">
				<div className="card-header">
					Author: {name}
				</div>
				<Link className="p-1" to={`/single/${id}`}>
			  	<img
			  		className="card-img-top" alt="error"
			  		src={window.location.origin+'/uploads/'+image}
		  		/>
				</Link>
			  <div className="card-body">
			    <h5 className="card-title">{title}</h5>
			    <p className="card-text">
						{content.slice(0, 300)+'...'}
			    </p>
			    <Link className="card-link" to={`/single/${id}`}>Show all</Link>
			  </div>
			  <div className="card-footer text-muted d-flex justify-content-between">
				  <small>Last updated {last_update}</small>
				  <span>
				  	<i className="fa fa-eye" aria-hidden="true"></i>
				  	<span> {viewed}</span>
				  </span>
				</div>
			</div>
		);
	}
}

Article.propTypes = {
	article: PropTypes.object.isRequired
};

export default Article;