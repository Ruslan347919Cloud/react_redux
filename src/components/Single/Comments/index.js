import React from 'react';
import PropTypes from 'prop-types';
import Comment from './Comment';

class Comments extends React.Component {

	getCommentsList() {
		const {comments} = this.props;
		return comments.map(comment => <Comment
			comment={comment}
			key={comment.id}
		/>);
	}

	render() {
		return (
			<ul className="list-unstyled mt-4">
				<li>
					<h5 className="rounded-top bg-primary text-white shadow p-2">Comments</h5>
				</li>
			  {this.getCommentsList()}
			</ul>
		);
	}
}

Comments.propTypes = {
	comments: PropTypes.array.isRequired
};

export default Comments;