import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import ArticleFull from './ArticleFull';
import Comments from './Comments/';
import Pagination from '../Pagination/';
import {
	fetchSingleArticle,
	fetchComments
} from '../../actions/SingleActions';

class Single extends React.Component {

	constructor(props) {
		super(props);
		this.pageSize = 3;
	}

	componentDidMount() {
		const articleId = this.props.match.params.id;
		const {
			fetchSingleArticleAction,
			fetchCommentsAction
		} = this.props;
		fetchSingleArticleAction(articleId);
		fetchCommentsAction(
			articleId, this.getCurrentPage(), this.pageSize);
	}

	componentDidUpdate(prevProps, prevState, prevContext) {
		const {
			fetchSingleArticleAction,
			fetchCommentsAction
		} = this.props;
		const {id, page} = this.props.match.params;
		const {id: idPrev, page: pagePrev} = prevProps.match.params;
		if (id !== idPrev) {
			fetchSingleArticleAction(id);
			fetchCommentsAction(
				id, this.getCurrentPage(), this.pageSize);
		}
		if (page !== pagePrev) {
			fetchCommentsAction(
				id, this.getCurrentPage(), this.pageSize);
		}
	}

	getCurrentPage() {
		return (typeof this.props.match.params.page !== 'undefined')
			? Number(this.props.match.params.page) : 1;
	}

	render() {
		const {
			article,
			comments,
			pages
		} = this.props;
		const {id} = this.props.match.params;
		return (
			<div className="col-md-7 col-lg-8 articles">
				<ArticleFull article={article}/>
				{(comments.length > 0) && (<Comments comments={comments}/>)}
				{(pages.length > 1) && (<Pagination
					route={'/single/'}
					pages={pages}
					componentId={Number(id)}
					currentPage={this.getCurrentPage()}
				/>)}
			</div>
		);
	}
}

const mapStateToProps = store => {
  const {article, comments, pages} = store.single;
  return {
    article: article,
    comments: comments,
    pages: pages
  }
};

const mapDispatchToProps = dispatch => ({
  fetchSingleArticleAction:
  	(articleId) => dispatch(
  		fetchSingleArticle(articleId)),
	fetchCommentsAction:
  	(articleId, page, limit) => dispatch(
  		fetchComments(articleId, page, limit))
});

Single.propTypes = {
	article: PropTypes.object.isRequired,
	comments: PropTypes.array.isRequired,
	pages: PropTypes.array.isRequired,
	match: PropTypes.object.isRequired,
	fetchSingleArticleAction: PropTypes.func.isRequired,
	fetchCommentsAction: PropTypes.func.isRequired
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Single);