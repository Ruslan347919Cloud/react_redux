import React from 'react';

class Login extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			email: '',
			password: '',
			rememberMe: false
		}
	}

	submitHandler = event => {
		event.preventDefault();
		const baseUrl = `http://react.loc/index.php?r=auth-rest`;
		const action = '/login';
		const postData = JSON.stringify(this.state);

		fetch(baseUrl+action, {
			method: 'POST',
			body: '&json='+postData,
			headers: {
				'Content-Type':
					'application/x-www-form-urlencoded; charset=utf-8'
			},
			//credentials: 'include'
		}).then(response => response.json())
			.then(data => console.log(data))
			.catch(error => console.log(error.message));
	}

	changeFieldHandler = event => {
		const {name, value} = event.currentTarget;
		this.setState({
			[name]: value
		});

		/*let {name, value} = event.currentTarget;
		let data = {[name]: value};
		this.props.changeField(data);*/
	}

	changeCheckboxHandler = event => {
		const {checked} = event.currentTarget;
		this.setState({
			rememberMe: checked
		});
	}

	render() {
		const {email, password} = this.state;
		return (
			<div className="col-md-7 col-lg-8 articles">
				<form>
				  <div className="form-group">
				    <label htmlFor="loginEmail">Email address</label>
				    <input
				    	type="email"
				    	name="email"
				    	className="form-control"
				    	id="loginEmail"
				    	value={email}
				    	onChange={this.changeFieldHandler}
			    	/>
				  </div>
				  <div className="form-group">
				    <label htmlFor="loginPassword">Password</label>
				    <input
				    	type="password"
				    	name="password"
				    	className="form-control"
				    	id="loginPassword"
				    	value={password}
				    	onChange={this.changeFieldHandler}
			    	/>
				  </div>
				  <div className="form-group form-check">
				    <input
				    	type="checkbox"
				    	className="form-check-input"
				    	id="loginCheck"
				    	onChange={this.changeCheckboxHandler}
			    	/>
				    <label className="form-check-label" htmlFor="loginCheck">Remember me</label>
				  </div>
				  <button
				  	type="submit"
				  	className="btn btn-primary"
				  	onClick={this.submitHandler}
			  	>
				  	Login
			  	</button>
				</form>
			</div>
		);
	}
}

export default Login;