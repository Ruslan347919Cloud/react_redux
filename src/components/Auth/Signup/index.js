import React from 'react';

class Signup extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			name: '',
			email: '',
			password: ''
		}
	}

	submitHandler = event => {
		event.preventDefault();
		const baseUrl = `http://react.loc/index.php?r=auth-rest`;
		const action = '/signup';
		const postData = JSON.stringify('data');

		fetch(baseUrl+action, {
			method: 'POST',
			body: '&json='+postData,
			headers: {
				'Content-Type':
					'application/x-www-form-urlencoded; charset=utf-8'
			}
		}).then(response => response.json())
			.then(data => console.log(data))
			.catch(error => console.log(error.message));
	}

	changeFieldHandler = event => {
		const {name, value} = event.currentTarget;
		this.setState({
			[name]: value
		});
	}

	render() {
		const {name, email, password} = this.state;
		return (
			<div className="col-md-7 col-lg-8 articles">
				<form>
					<div className="form-group">
				    <label htmlFor="signupName">Name</label>
				    <input
				    	type="text"
				    	name="name"
				    	className="form-control"
				    	id="signupName"
				    	value={name}
				    	onChange={this.changeFieldHandler}
			    	/>
				  </div>
				  <div className="form-group">
				    <label htmlFor="signupEmail">Email address</label>
				    <input
				    	type="email"
				    	name="email"
				    	className="form-control"
				    	id="signupEmail"
				    	aria-describedby="emailHelp"
				    	value={email}
				    	onChange={this.changeFieldHandler}
			    	/>
				    <small id="emailHelp" className="form-text text-muted">We'll never share your email with anyone else.</small>
				  </div>
				  <div className="form-group">
				    <label htmlFor="signupPassword">Password</label>
				    <input
				    	type="password"
				    	name="password"
				    	className="form-control"
				    	id="signupPassword"
				    	value={password}
				    	onChange={this.changeFieldHandler}
			    	/>
				  </div>
				  <button
				  	type="submit"
				  	className="btn btn-primary"
				  	onClick={this.submitHandler}
			  	>
				  	Signup
			  	</button>
				</form>
			</div>
		);
	}
}

export default Signup;