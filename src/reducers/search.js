import {
	CHANGE_STATE,
	SEND_QUERY
} from '../actions/SearchActions';

const initialState = {
	keyword: '',
	result: {}
};

export const searchReducer = function(state = initialState, action) {
	switch (action.type) {
		case CHANGE_STATE:
			return {...state, keyword: action.payload};
		case SEND_QUERY:
			return {...state, result: action.payload};
		default: return state;
	}
};