import {FETCH_SIDEBAR_DATA} from '../actions/SidebarActions';

const initialState = {
	latest: [],
	categories: [],
	authors: [],
	tags: []
};

export const sidebarReducer = function(state = initialState, action) {
	switch (action.type) {
		case FETCH_SIDEBAR_DATA:
			return {
				...state,
				...action.payload
			};
		default: return state;
	}
};