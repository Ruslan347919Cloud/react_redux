import {combineReducers} from 'redux';
import {articlesReducer} from './articles';
import {sidebarReducer} from './sidebar';
import {singleReducer} from './single';
import {sortReducer} from './sort';
import {searchReducer} from './search';
import {userFormReducer} from './admin/User/userForm';
import {userViewReducer} from './admin/User/userView';
import {categoryViewReducer} from './admin/Category/categoryView';
import {categoryFormReducer} from './admin/Category/categoryForm';
import {articleViewReducer} from './admin/Article/articleView';
import {articleFormReducer} from './admin/Article/articleForm';
import {setAttributesReducer} from './admin/Article/setAttributes';
import {tagViewReducer} from './admin/Tag/tagView';
import {tagFormReducer} from './admin/Tag/tagForm';
import {commentViewReducer} from './admin/Comment/commentView';
import {commentFormReducer} from './admin/Comment/commentForm';
import {listReducer} from './admin/list';
import {reducer as formReducer} from 'redux-form';

export const rootReducer = combineReducers({
	sidebar: sidebarReducer,
	articles: articlesReducer,
	single: singleReducer,
	sort: sortReducer,
	search: searchReducer,
	userForm: userFormReducer,
	userView: userViewReducer,
	categoryView: categoryViewReducer,
	categoryForm: categoryFormReducer,
	articleView: articleViewReducer,
	setAttributes: setAttributesReducer,
	articleForm: articleFormReducer,
	tagView: tagViewReducer,
	tagForm: tagFormReducer,
	commentView: commentViewReducer,
	commentForm: commentFormReducer,
	list: listReducer,
	form: formReducer
});