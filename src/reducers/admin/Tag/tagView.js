import {
	FETCH_SINGLE_TAG
} from '../../../actions/admin/Tag/TagViewActions';

const initialState = {
	tag: {},
	articles: [],
	pages: []
};

export const tagViewReducer = function(state = initialState, action) {
	switch (action.type) {
		case FETCH_SINGLE_TAG:
			return {
				...state,
				...action.payload
			};
		default: return state;
	}
};