import {
	CREATE_ITEM,
	FIND_ITEM,
	UPDATE_ITEM,
	CLEAR_FORM
} from '../../../actions/admin/FormActions';

const initialState = {
	currentTagId: '',
	currentTagTitle: '',
	newTagData: {}
};

export const tagFormReducer = function(state = initialState, action) {
	switch (action.type) {
		case FIND_ITEM:
			return {
				...state,
				currentTagId: action.payload.id,
				currentTagTitle: action.payload.title
			};
		case CREATE_ITEM:
			return {
				...state,
				newTagData: action.payload
			};
		case UPDATE_ITEM:
			return {
				...state,
				newTagData: action.payload
			};
		case CLEAR_FORM:
			return {
				...state,
				currentTagId: action.payload.defaultValue,
				currentTagTitle: action.payload.defaultValue,
				newTagData: action.payload.defaultItemData
			}
		default: return state;
	}
}