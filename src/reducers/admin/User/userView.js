import {FETCH_SINGLE_USER} from '../../../actions/admin/User/UserViewActions.js';

const initialState = {
	user: {},
	articles: [],
	pages: []
};

export const userViewReducer = function(state = initialState, action) {
	switch (action.type) {
		case FETCH_SINGLE_USER:
			return {
				...state,
				...action.payload
			};
		default: return state;
	}
};