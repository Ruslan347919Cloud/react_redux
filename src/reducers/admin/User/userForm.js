import {
	CREATE_ITEM,
	FIND_ITEM,
	UPDATE_ITEM,
	CLEAR_FORM
} from '../../../actions/admin/FormActions';

const initialState = {
	currentUserId: '',
	currentUserName: '',
	currentUserEmail: '',
	currentUserPassword: '',
	newUserData: {}
};

export const userFormReducer = function(state = initialState, action) {
	switch (action.type) {
		case FIND_ITEM:
			return {
				...state,
				currentUserId: action.payload.id,
				currentUserName: action.payload.name,
				currentUserEmail: action.payload.email,
				currentUserPassword: action.payload.password
			};
		case CREATE_ITEM:
			return {
				...state,
				newUserData: action.payload
			};
		case UPDATE_ITEM:
			return {
				...state,
				newUserData: action.payload
			};
		case CLEAR_FORM:
			return {
				...state,
				currentUserId: action.payload.defaultValue,
				currentUserName: action.payload.defaultValue,
				currentUserEmail: action.payload.defaultValue,
				currentUserPassword: action.payload.defaultValue,
				newUserData: action.payload.defaultItemData
			}
		default: return state;
	}
}