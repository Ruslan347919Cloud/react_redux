import {
	CREATE_ITEM,
	FIND_ITEM,
	UPDATE_ITEM,
	CLEAR_FORM
} from '../../../actions/admin/FormActions';

const initialState = {
	currentCommentId: '',
	currentCommentText: '',
	currentCommentUserId: '',
	currentCommentArticleId: '',
	newCommentData: {}
};

export const commentFormReducer = function(state = initialState, action) {
	switch (action.type) {
		case FIND_ITEM:
			return {
				...state,
				currentCommentId: action.payload.id,
				currentCommentText: action.payload.text,
				currentCommentUserId: action.payload.user_id,
				currentCommentArticleId: action.payload.article_id
			};
		case CREATE_ITEM:
			return {
				...state,
				newCommentData: action.payload
			};
		case UPDATE_ITEM:
			return {
				...state,
				newCommentData: action.payload
			};
		case CLEAR_FORM:
			return {
				...state,
				currentCommentId: action.payload.defaultValue,
				currentCommentText: action.payload.defaultValue,
				currentCommentUserId: action.payload.defaultValue,
				currentCommentArticleId: action.payload.defaultValue,
				newCommentData: action.payload.defaultItemData
			}
		default: return state;
	}
}