import {
	FETCH_SINGLE_COMMENT,
	CHANGE_COMMENT_STATUS
} from '../../../actions/admin/Comment/CommentViewActions';

const initialState = {
	comment: {},
	status: null
};

export const commentViewReducer = function(state = initialState, action) {
	switch (action.type) {
		case FETCH_SINGLE_COMMENT:
			return {
				...state,
				comment: action.payload
			};
		case CHANGE_COMMENT_STATUS:
			return {
				...state,
				status: action.payload
			};
		default: return state;
	}
};