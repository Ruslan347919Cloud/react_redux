import {
	FETCH_ITEMS,
	DELETE_ITEM,
	CHANGE_ITEM_STATUS
} from '../../actions/admin/ListActions';

const initialState = {
	items: [],
	pages: [],
	deletedItemId: '',
	changedItem: {}
};

export const listReducer = function(state = initialState, action) {
	switch (action.type) {
		case FETCH_ITEMS:
			return {
				...state,
				items: action.payload.items,
				pages: action.payload.pages,
				deletedItemId: ''
			};
		case DELETE_ITEM:
			return {
				...state,
				deletedItemId: action.payload
			};
		case CHANGE_ITEM_STATUS:
			return {
				...state,
				changedItem: action.payload
			};
		default: return state;
	}
};