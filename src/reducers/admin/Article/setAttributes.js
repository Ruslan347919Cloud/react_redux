import {
	SET_CATEGORIES,
	SET_TAGS
} from '../../../actions/admin/Article/SetAttributesActions';

const initialState = {
	categories: [],
	tags: []
};

export const setAttributesReducer = function(state = initialState, action) {
	switch (action.type) {
		case SET_CATEGORIES:
			return {
				...state,
				categories: action.payload
			};
		case SET_TAGS:
			return {
				...state,
				tags: action.payload
			};
		default: return state;
	}
};
