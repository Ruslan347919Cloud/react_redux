import {
	FETCH_SINGLE_ARTICLE,
	CHANGE_ARTICLE_STATUS,
	SET_CATEGORIES,
	SET_TAGS
} from '../../../actions/admin/Article/ArticleViewActions';

const initialState = {
	article: {},
	comments: [],
	pages: [],
	status: null
};

export const articleViewReducer = function(state = initialState, action) {
	switch (action.type) {
		case FETCH_SINGLE_ARTICLE:
			return {
				...state,
				...action.payload
			};
		case CHANGE_ARTICLE_STATUS:
			return {
				...state,
				status: action.payload
			};
		default: return state;
	}
};