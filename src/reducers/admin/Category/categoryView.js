import {
	FETCH_SINGLE_CATEGORY
} from '../../../actions/admin/Category/CategoryViewActions';

const initialState = {
	category: {},
	articles: [],
	pages: []
};

export const categoryViewReducer = function(state = initialState, action) {
	switch (action.type) {
		case FETCH_SINGLE_CATEGORY:
			return {
				...state,
				...action.payload
			};
		default: return state;
	}
};