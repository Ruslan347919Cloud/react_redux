import {
	CREATE_ITEM,
	FIND_ITEM,
	UPDATE_ITEM,
	CLEAR_FORM
} from '../../../actions/admin/FormActions';

const initialState = {
	currentCategoryId: '',
	currentCategoryTitle: '',
	newCategoryData: {}
};

export const categoryFormReducer = function(state = initialState, action) {
	switch (action.type) {
		case FIND_ITEM:
			return {
				...state,
				currentCategoryId: action.payload.id,
				currentCategoryTitle: action.payload.title
			};
		case CREATE_ITEM:
			return {
				...state,
				newCategoryData: action.payload
			};
		case UPDATE_ITEM:
			return {
				...state,
				newCategoryData: action.payload
			};
		case CLEAR_FORM:
			return {
				...state,
				currentCategoryId: action.payload.defaultValue,
				currentCategoryTitle: action.payload.defaultValue,
				newCategoryData: action.payload.defaultItemData
			}
		default: return state;
	}
}