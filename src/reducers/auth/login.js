import {
	LOGIN,
	CHANGE_FIELD,
	CLEAR_FORM
} from '../../actions/auth/LoginActions';

const initialState = {
	email: '',
	password: '',
	rememberMe: false
};

export const loginReducer = function(state = initialState, action) {
	switch (action.type) {
		case CHANGE_FIELD: {
			let keys = Object.keys(action.payload);
			let values = Object.values(action.payload);
			return {
				...state,
				[keys[0]]: values[0]
			};
		};
		case CLEAR_FORM:
			return {
				...state,
				updatingUserId: action.payload.updatingUserId,
				name: action.payload.name,
				email: action.payload.email,
				password: action.payload.password,
				newUserData: action.payload.newUserData
			}
		default: return state;
	}
}