import {SORT_BY} from '../actions/SortActions';

const initialState = {
	articles: [],
	pages: []
};

export const sortReducer = function(state = initialState, action) {
	switch (action.type) {
		case SORT_BY:
			return {
				...state,
				...action.payload
			};
		default: return state;
	}
};