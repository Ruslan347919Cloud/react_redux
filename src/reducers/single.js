import {
	FETCH_SINGLE_ARTICLE,
	FETCH_COMMENTS
} from '../actions/SingleActions';

const initialState = {
	article: {},
	comments: [],
	pages: []
};

export const singleReducer = function(state = initialState, action) {
	switch (action.type) {
		case FETCH_SINGLE_ARTICLE:
			return {...state,
				article: action.payload.article
			};
		case FETCH_COMMENTS:
			return {...state,
				comments: action.payload.comments,
				pages: action.payload.pages
			};
		default: return state;
	}
};