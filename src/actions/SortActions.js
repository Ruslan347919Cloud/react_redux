export const SORT_BY = 'SORT_BY';

export const sortBy = (model, id, page, limit) => dispatch => {
	let baseUrl = `http://react.loc/index.php?r=site-rest`;
	let action = '';
	let body = `&id=${id}&page=${page}&limit=${limit}`;
	switch (model) {
		case 'authors':
			action = '/user-articles';
		break;
		case 'categories':
			action = '/category-articles';
		break;
		case 'tags':
			action = '/tag-articles';
	}
	fetch(baseUrl+action+body)
		.then(response => response.json())
		.then(data => dispatch({
			type: SORT_BY,
			payload: data
		}));
};