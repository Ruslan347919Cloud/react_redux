export const FETCH_ARTICLES = 'FETCH_ARTICLES';

export const fetchArticles = (page, limit) => dispatch => {
	const baseUrl = `http://react.loc/index.php?r=site-rest`;
	const action = '/all-articles';
	const body = `&page=${page}&limit=${limit}`;
	fetch(baseUrl+action+body)
		.then(response => response.json())
		.then(data => dispatch({
			type: FETCH_ARTICLES,
			payload: data
		}));
};