export const FETCH_SINGLE_ARTICLE = 'FETCH_SINGLE_ARTICLE';
export const FETCH_COMMENTS = 'FETCH_COMMENTS';

export const fetchSingleArticle = articleId => dispatch => {
	const baseUrl = `http://react.loc/index.php?r=site-rest`;
	const action = '/single-article';
	const body = `&id=${articleId}`;
		fetch(baseUrl+action+body)
			.then(response => response.json())
			.then(data => dispatch({
				type: FETCH_SINGLE_ARTICLE,
				payload: data
			}));
}

export const fetchComments = (articleId, page, limit) => dispatch => {
	const baseUrl = `http://react.loc/index.php?r=site-rest`;
	const action = '/article-comments';
	const body = `&id=${articleId}&page=${page}&limit=${limit}`;
		fetch(baseUrl+action+body)
			.then(response => response.json())
			.then(data => dispatch({
				type: FETCH_COMMENTS,
				payload: data
			}));
}