export const LOGIN = 'LOGIN';
export const CHANGE_FIELD = 'CHANGE_FIELD';
export const CLEAR_FORM = 'CLEAR_FORM';

export const login = data => dispatch => {
	const baseUrl = `http://react.loc/index.php?r=admin/user`;
	const action = '/create';
	const postData = JSON.stringify(data);

	fetch(baseUrl+action, {
		method: 'POST',
		body: '&json='+postData,
		headers: {
			'Content-Type':
				'application/x-www-form-urlencoded; charset=utf-8'
		}
	}).then(response => response.json())
		.then(data => dispatch({
			type: LOGIN,
			payload: data
		}))
		.catch(error => console.log(error.message));
};

export const changeField = function(data) {
	return {
		type: CHANGE_FIELD,
		payload: data
	};
};

export const clearForm = function() {
	return {
		type: CLEAR_FORM,
		payload: {
			email: '',
			password: '',
			rememberMe: false
		}
	};
};