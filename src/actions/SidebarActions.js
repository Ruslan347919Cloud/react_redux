export const FETCH_SIDEBAR_DATA = 'FETCH_SIDEBAR_DATA';

export const fetchSidebarData = () => dispatch => {
	const url = `http://react.loc/index.php?r=site-rest/sidebar`;
	fetch(url)
		.then(response => response.json())
		.then(data => dispatch({
			type: FETCH_SIDEBAR_DATA,
			payload: data
		}));
}