export const FETCH_ITEMS = 'FETCH_ITEMS';
export const DELETE_ITEM = 'DELETE_ITEM';
export const CHANGE_ITEM_STATUS = 'CHANGE_ITEM_STATUS';

export const fetchItems = (page, limit, controller) => dispatch => {
	const baseUrl = `http://react.loc/index.php?r=admin/${controller}`;
	const action = '/index';
	const body = `&page=${page}&limit=${limit}`;
	fetch(baseUrl+action+body)
		.then(response => response.json())
		.then(data => dispatch({
			type: FETCH_ITEMS,
			payload: data
		}));
};

export const changeItemStatus = (id, controller) => dispatch => {
	const baseUrl = `http://react.loc/index.php?r=admin/${controller}`;
	const action = '/change-status';
	const body = `&id=${id}`;
	fetch(baseUrl+action+body)
		.then(response => response.json())
		.then(status => dispatch({
			type: CHANGE_ITEM_STATUS,
			payload: {
				id: id,
				visible: status
			}
		}));
};

export const deleteItem = (id, controller) => dispatch => {
	const baseUrl = `http://react.loc/index.php?r=admin/${controller}`;
	const action = '/delete';
	fetch(baseUrl+action, {
		method: 'POST',
		body: `&id=${id}`,
		headers: {
			'Content-Type':
				'application/x-www-form-urlencoded; charset=utf-8'
		}
	}).then(response => response.json())
		.then(data => dispatch({
			type: DELETE_ITEM,
			payload: data
		}))
		.catch(error => console.log(error.message));
}