export const CREATE_ITEM = 'CREATE_ITEM';
export const FIND_ITEM = 'FIND_ITEM';
export const UPDATE_ITEM = 'UPDATE_ITEM';
export const CLEAR_FORM = 'CLEAR_FORM';

export const findItem = (id, controller) => dispatch => {
	const baseUrl = `http://react.loc/index.php?r=admin/${controller}`;
	const action = '/update';
	const body = `&id=${id}`;
	fetch(baseUrl+action+body)
		.then(response => response.json())
		.then(data => dispatch({
			type: FIND_ITEM,
			payload: data
		}));
};

export const updateItem = (data, controller) => dispatch => {
	const baseUrl = `http://react.loc/index.php?r=admin/${controller}`;
	const action = '/update';
	const postData = JSON.stringify(data);

	fetch(baseUrl+action, {
		method: 'POST',
		body: '&json='+postData,
		headers: {
			'Content-Type':
				'application/x-www-form-urlencoded; charset=utf-8'
		}
	}).then(response => response.json())
		.then(data => dispatch({
			type: UPDATE_ITEM,
			payload: data
		}))
		.catch(error => console.log(error.message));
};

export const createItem = (data, controller) => dispatch => {
	const baseUrl = `http://react.loc/index.php?r=admin/${controller}`;
	const action = '/create';
	const postData = JSON.stringify(data);

	fetch(baseUrl+action, {
		method: 'POST',
		body: '&json='+postData,
		headers: {
			'Content-Type':
				'application/x-www-form-urlencoded; charset=utf-8'
		}
	}).then(response => response.json())
		.then(data => dispatch({
			type: CREATE_ITEM,
			payload: data
		}))
		.catch(error => console.log(error.message));
};

export const clearForm = function() {
	return {
		type: CLEAR_FORM,
		payload: {
			defaultValue: '',
			defaultItemData: {}
		}
	};
};