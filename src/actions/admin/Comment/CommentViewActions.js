export const FETCH_SINGLE_COMMENT = 'FETCH_SINGLE_COMMENT';
export const CHANGE_COMMENT_STATUS = 'CHANGE_COMMENT_STATUS';

export const fetchSingleComment = (id) => dispatch => {
	const baseUrl = `http://react.loc/index.php?r=admin/comment`;
	const action = '/view';
	const body = `&id=${id}`;
	fetch(baseUrl+action+body)
		.then(response => response.json())
		.then(data => dispatch({
			type: FETCH_SINGLE_COMMENT,
			payload: data
		}));
};

export const changeCommentStatus = id => dispatch => {
	const baseUrl = `http://react.loc/index.php?r=admin/comment`;
	const action = '/change-status';
	const body = `&id=${id}`;
	fetch(baseUrl+action+body)
		.then(response => response.json())
		.then(data => dispatch({
			type: CHANGE_COMMENT_STATUS,
			payload: data
		}));
};