export const FETCH_SINGLE_ARTICLE = 'FETCH_SINGLE_ARTICLE';
export const CHANGE_ARTICLE_STATUS = 'CHANGE_ARTICLE_STATUS';

export const fetchSingleArticle = (id, page, limit) => dispatch => {
	const baseUrl = `http://react.loc/index.php?r=admin/article`;
	const action = '/view';
	const body = `&id=${id}&page=${page}&limit=${limit}`;
	fetch(baseUrl+action+body)
		.then(response => response.json())
		.then(data => dispatch({
			type: FETCH_SINGLE_ARTICLE,
			payload: data
		}));
};

export const changeArticleStatus = id => dispatch => {
	const baseUrl = `http://react.loc/index.php?r=admin/article`;
	const action = '/change-status';
	const body = `&id=${id}`;
	fetch(baseUrl+action+body)
		.then(response => response.json())
		.then(data => dispatch({
			type: CHANGE_ARTICLE_STATUS,
			payload: data
		}));
};