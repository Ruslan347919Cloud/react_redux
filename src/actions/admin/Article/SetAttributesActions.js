export const SET_CATEGORIES = 'SET_CATEGORIES';
export const SET_TAGS = 'SET_TAGS';

export const setCategories = (id, categories) => dispatch => {
	const baseUrl = `http://react.loc/index.php?r=admin/article`;
	const action = '/set-categories';
	//const body = `&id=${id}`;
	/*fetch(baseUrl+action+body)
		.then(response => response.json())
		.then(data => dispatch({
			type: CHANGE_ARTICLE_STATUS,
			payload: data
		}));*/
		const body = {
			id: id,
			categories: categories
		};
		fetch(baseUrl+action, {
			method: 'POST',
			body: '&json='+JSON.stringify(body),
			headers: {
				'Content-Type':
					'application/x-www-form-urlencoded; charset=utf-8'
			}
		})
		.then(response => response.json())
		.then(data => console.log(data));
};

export const setTags = (id, tags) => dispatch => {
	const baseUrl = `http://react.loc/index.php?r=admin/article`;
	const action = '/set-tags';
	//const body = `&id=${id}`;
	/*fetch(baseUrl+action+body)
		.then(response => response.json())
		.then(data => dispatch({
			type: CHANGE_ARTICLE_STATUS,
			payload: data
		}));*/
		const body = {
			id: id,
			tags: tags
		};
		fetch(baseUrl+action, {
			method: 'POST',
			body: '&json='+JSON.stringify(body),
			headers: {
				'Content-Type':
					'application/x-www-form-urlencoded; charset=utf-8'
			}
		})
		.then(response => response.json())
		.then(data => console.log(data));
};