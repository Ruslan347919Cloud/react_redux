export const FETCH_SINGLE_TAG = 'FETCH_SINGLE_TAG';

export const fetchSingleTag = (id, page, limit) => dispatch => {
	const baseUrl = `http://react.loc/index.php?r=admin/tag`;
	const action = '/view';
	const body = `&id=${id}&page=${page}&limit=${limit}`;
	fetch(baseUrl+action+body)
		.then(response => response.json())
		.then(data => dispatch({
			type: FETCH_SINGLE_TAG,
			payload: data
		}));
};