export const FETCH_SINGLE_USER = 'FETCH_SINGLE_USER';

export const fetchSingleUser = (id, page, limit) => dispatch => {
	const baseUrl = `http://react.loc/index.php?r=admin/user`;
	const action = '/view';
	const body = `&id=${id}&page=${page}&limit=${limit}`;
	fetch(baseUrl+action+body)
		.then(response => response.json())
		.then(data => dispatch({
			type: FETCH_SINGLE_USER,
			payload: data
		}));
};