export const FETCH_SINGLE_CATEGORY = 'FETCH_SINGLE_CATEGORY';

export const fetchSingleCategory = (id, page, limit) => dispatch => {
	const baseUrl = `http://react.loc/index.php?r=admin/category`;
	const action = '/view';
	const body = `&id=${id}&page=${page}&limit=${limit}`;
	fetch(baseUrl+action+body)
		.then(response => response.json())
		.then(data => dispatch({
			type: FETCH_SINGLE_CATEGORY,
			payload: data
		}));
};