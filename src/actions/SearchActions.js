export const CHANGE_STATE = 'CHANGE_STATE';
export const SEND_QUERY = 'SEND_QUERY';

export const changeState = keyword => ({
	type: CHANGE_STATE,
	payload: keyword
});

export const sendQuery = keyword => dispatch => {
	const baseUrl = `http://react.loc/index.php?r=site-rest`;
	const action = '/search';
	const body = '&searchQuery='+keyword;
	fetch(baseUrl+action+body)
		.then(response => response.json())
		.then(result => dispatch({
			type: SEND_QUERY,
			payload: result
		}));
};