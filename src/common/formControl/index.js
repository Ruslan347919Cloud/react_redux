import React from 'react';

export const Input = ({input, meta, ...props}) => {
	const hasError = meta.touched && meta.error;
	const errorStyle = ' border border-danger';
	return (
		<div>
			<input {...input} {...props}
				className={
					props.className + (hasError ? errorStyle : '')
				}
			/>
			{hasError && <span>{meta.error}</span>}
		</div>
	);
}

export const TextArea = ({input, meta, ...props}) => {
	const hasError = meta.touched && meta.error;
	const errorStyle = ' border border-danger';
	return (
		<div>
			<textarea {...input} {...props}
				className={
					props.className + (hasError ? errorStyle : '')
				}
			/>
			{hasError && <span>{meta.error}</span>}
		</div>
	);
}